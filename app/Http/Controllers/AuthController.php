<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.sign_up');
    }

    public function welcome(){
        return view('halaman.welcome');
    }

    public function kirim(Request $request){
        $Nama = $request['First_Name'];
        $Belakang = $request['Last_Name'];
        return view('halaman.welcome',compact('Nama','Belakang'));
    
    }
}
