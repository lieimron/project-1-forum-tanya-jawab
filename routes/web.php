<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('/', 'IndexController@index');

Route::get('/home', 'IndexController@index');

route::get('/sign_up', 'AuthController@register');

route::post('/welcome', 'AuthController@kirim');

route::get('/data-table', function(){
    return view('table.data-table');
});

// CRUD Kategori
route::get('/kategori/create','KategoriController@create');